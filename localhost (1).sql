-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 28 2019 г., 10:32
-- Версия сервера: 5.7.27-0ubuntu0.16.04.1
-- Версия PHP: 7.0.33-0ubuntu0.16.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `db_gitmysite`
--
CREATE DATABASE IF NOT EXISTS `db_gitmysite` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `db_gitmysite`;

-- --------------------------------------------------------

--
-- Структура таблицы `addresses`
--

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `address` blob NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `addresses`
--

INSERT INTO `addresses` (`id`, `id_user`, `address`, `type`) VALUES
(141, 4, 0x613a343a7b733a373a22636f756e747279223b733a31313a2250d0bed181d181d0b8d18f223b733a343a2263697479223b733a31313a224dd0bed181d0bad0b2d0b0223b733a363a22737472656574223b733a31313a224bd0b5d0bdd0b8d0bdd0b0223b733a363a226e756d626572223b733a323a223433223b7d, 1),
(142, 4, 0x613a343a7b733a373a22636f756e747279223b733a31313a2250d0bed181d181d0b8d18f223b733a343a2263697479223b733a31313a224dd0bed181d0bad0b2d0b0223b733a363a22737472656574223b733a31313a224bd0b5d0bdd0b8d0bdd0b0223b733a363a226e756d626572223b733a313a2234223b7d, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `basket`
--

CREATE TABLE `basket` (
  `id_basket` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_good` int(11) NOT NULL,
  `qt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `basket`
--

INSERT INTO `basket` (`id_basket`, `id_user`, `id_good`, `qt`) VALUES
(1, 4, 3, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `favorites`
--

CREATE TABLE `favorites` (
  `id_favorites` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_good` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `favorites`
--

INSERT INTO `favorites` (`id_favorites`, `id_user`, `id_good`) VALUES
(35, 4, 12),
(36, 4, 3),
(37, 6, 1),
(38, 6, 4),
(39, 6, 12),
(40, 6, 21);

-- --------------------------------------------------------

--
-- Структура таблицы `goods`
--

CREATE TABLE `goods` (
  `id` int(11) NOT NULL,
  `catid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_desc` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `img` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `goods`
--

INSERT INTO `goods` (`id`, `catid`, `name`, `short_desc`, `description`, `img`, `price`) VALUES
(1, 'hob', 'Варочная поверхность LG 3000', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.', 'Повседневная практика показывает, что постоянный количественный рост и сфера нашей активности обеспечивает широкому кругу (специалистов) участие в формировании существенных финансовых и административных условий. Задача организации, в особенности же новая модель организационной деятельности способствует подготовки и реализации форм развития. Таким образом реализация намеченных плановых заданий влечет за собой процесс внедрения и модернизации направлений прогрессивного развития. Таким образом сложившаяся структура организации обеспечивает широкому кругу (специалистов) участие в формировании соответствующий условий активизации. С другой стороны реализация намеченных плановых заданий способствует подготовки и реализации модели развития. Не следует, однако забывать, что рамки и место обучения кадров представляет собой интересный эксперимент проверки форм развития.', 'images/goods/vp/1.jpg', 2000),
(2, 'hob', 'Варочная поверхность LG 2000', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.', 'Повседневная практика показывает, что постоянный количественный рост и сфера нашей активности обеспечивает широкому кругу (специалистов) участие в формировании существенных финансовых и административных условий. Задача организации, в особенности же новая модель организационной деятельности способствует подготовки и реализации форм развития. Таким образом реализация намеченных плановых заданий влечет за собой процесс внедрения и модернизации направлений прогрессивного развития. Таким образом сложившаяся структура организации обеспечивает широкому кругу (специалистов) участие в формировании соответствующий условий активизации. С другой стороны реализация намеченных плановых заданий способствует подготовки и реализации модели развития. Не следует, однако забывать, что рамки и место обучения кадров представляет собой интересный эксперимент проверки форм развития.', 'images/goods/vp/2.jpg', 3000),
(3, 'hob', 'Варочная поверхность BQ 3.0', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.', 'Равным образом укрепление и развитие структуры влечет за собой процесс внедрения и модернизации новых предложений. Повседневная практика показывает, что дальнейшее развитие различных форм деятельности позволяет выполнять важные задания по разработке системы обучения кадров, соответствует насущным потребностям. Идейные соображения высшего порядка, а также новая модель организационной деятельности представляет собой интересный эксперимент проверки позиций, занимаемых участниками в отношении поставленных задач.', 'images/goods/vp/3.jpg', 5000),
(4, 'hob', 'Варочная поверхность BQ 2.1', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.', 'Повседневная практика показывает, что постоянный количественный рост и сфера нашей активности в значительной степени обуславливает создание направлений прогрессивного развития. Не следует, однако забывать, что постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки форм развития. Идейные соображения высшего порядка, а также реализация намеченных плановых заданий способствует подготовки и реализации существенных финансовых и административных условий.', 'images/goods/vp/4.jpg', 7000),
(5, 'hob', 'Варочная поверхность DNS H221', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.', 'Товарищи! дальнейшее развитие различных форм деятельности представляет собой интересный эксперимент проверки новых предложений. Равным образом рамки и место обучения кадров требуют определения и уточнения модели развития. Разнообразный и богатый опыт новая модель организационной деятельности обеспечивает широкому кругу (специалистов) участие в формировании дальнейших направлений развития. Разнообразный и богатый опыт начало повседневной работы по формированию позиции требуют определения и уточнения существенных финансовых и административных условий. Значимость этих проблем настолько очевидна, что реализация намеченных плановых заданий способствует подготовки и реализации системы обучения кадров, соответствует насущным потребностям.', 'images/goods/vp/5.jpg', 5000),
(6, 'fridge', 'Холодильник Aristone PNG30', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/h/1.jpg', 10000),
(7, 'fridge', 'Холодильник LG pro', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretiumLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/h/2.jpg', 12000),
(8, 'fridge', 'Холодильник DNS tronic', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/h/3.jpg', 34300),
(9, 'fridge', 'Холодильник Мороз', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu. Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.', 'images/goods/h/4.jpg', 25000),
(10, 'fridge', 'Холодильник Холодос', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.', 'images/goods/h/5.jpg', 23300),
(11, 'oven', 'Духовой шкаф Rolsen RLS300S', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.\', \n', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/ds/3.jpg', 12000),
(12, 'oven', 'Духовой шкаф Rolsen RLS500DS', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/ds/1.jpg', 21000),
(13, 'oven', 'Духовой шкаф Rolsen RLS800DS', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretiumLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/ds/2.jpg', 23000),
(14, 'oven', 'Духовой шкаф Rolsen RLS50', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/ds/4.jpg', 22000),
(15, 'oven', 'Духовой шкаф Rolsen RLS9000DS', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ', 'images/goods/ds/5.jpg', 12100),
(16, 'washer', 'Стиральная машина Indesit Triple 43', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu.', 'images/goods/sm/1.jpg', 3000),
(17, 'washer', 'Стиральная машина Indesit Triple 300', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu.', 'images/goods/sm/2.jpg', 5000),
(18, 'washer', 'Стиральная машина Indesit Triple 40', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu.', 'images/goods/sm/3.jpg', 5500),
(19, 'washer', 'Стиральная машина Indesit Triple 555', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu.', 'images/goods/sm/4.jpg', 6700),
(20, 'washer', 'Стиральная машина Indesit Trip2', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu.', 'images/goods/sm/5.jpg', 6700),
(21, 'oven', 'Духовой шкаф Rolsen RLS3', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.\', \r\n', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/ds/6.jpg', 6000),
(22, 'oven', 'Духовой шкаф Rolsen RL40', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.\', \r\n', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/ds/7.jpg', 7000),
(23, 'oven', 'Духовой шкаф Rolsen RLtrP', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.\', \r\n', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/ds/8.jpg', 8000),
(24, 'oven', 'Духовой шкаф Rolsen RLS90', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.\', \r\n', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/ds/9.jpg', 999),
(25, 'oven', 'Духовой шкаф Rolsen R2000', 'Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного развития.\', \r\n', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/ds/10.jpg', 10100);

-- --------------------------------------------------------

--
-- Структура таблицы `goods_order`
--

CREATE TABLE `goods_order` (
  `id` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_good` int(11) NOT NULL,
  `qt` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `goods_order`
--

INSERT INTO `goods_order` (`id`, `id_order`, `id_user`, `id_good`, `qt`, `price`, `name`) VALUES
(21, 15, 6, 21, 2, 6000, 'Духовой шкаф Rolsen RLS3'),
(22, 15, 6, 12, 1, 21000, 'Духовой шкаф Rolsen RLS500DS'),
(23, 15, 6, 4, 2, 7000, 'Варочная поверхность BQ 2.1'),
(24, 15, 6, 1, 1, 2000, 'Варочная поверхность LG 3000'),
(28, 16, 6, 21, 3, 6000, 'Духовой шкаф Rolsen RLS3'),
(29, 16, 6, 4, 3, 7000, 'Варочная поверхность BQ 2.1');

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id_order` int(11) NOT NULL,
  `address` blob NOT NULL,
  `date` datetime NOT NULL DEFAULT '2010-10-10 10:10:10',
  `total` int(11) NOT NULL,
  `customer_data` blob NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id_order`, `address`, `date`, `total`, `customer_data`, `id_user`) VALUES
(15, 0x613a323a7b733a363a22747970655f31223b613a343a7b733a373a22636f756e747279223b733a31323a22d180d0bed181d181d0b8d18f223b733a343a2263697479223b733a31323a22d0bcd0bed181d0bad0b2d0b0223b733a363a22737472656574223b733a31323a22d0bbd0b5d0bdd0b8d0bdd0b0223b733a363a226e756d626572223b733a313a2231223b7d733a363a22747970655f32223b613a343a7b733a373a22636f756e747279223b733a31323a22d180d0bed181d181d0b8d18f223b733a343a2263697479223b733a31323a22d0bcd0bed181d0bad0b2d0b0223b733a363a22737472656574223b733a31323a22d0bbd0b5d0bdd0b8d0bdd0b0223b733a363a226e756d626572223b733a313a2231223b7d7d, '2019-10-28 10:27:35', 49000, 0x613a333a7b733a343a226e616d65223b733a363a22536572676579223b733a373a227375726e616d65223b733a363a224976616e6f76223b733a353a22656d61696c223b733a31393a227272726b737372727240676d61696c2e636f6d223b7d, 6),
(16, 0x613a323a7b733a363a22747970655f31223b613a343a7b733a373a22636f756e747279223b733a31323a22d180d0bed181d181d0b8d18f223b733a343a2263697479223b733a31323a22d0bcd0bed181d0bad0b2d0b0223b733a363a22737472656574223b733a31323a22d0bbd0b5d0bdd0b8d0bdd0b0223b733a363a226e756d626572223b733a313a2231223b7d733a363a22747970655f32223b613a343a7b733a373a22636f756e747279223b733a31323a22d180d0bed181d181d0b8d18f223b733a343a2263697479223b733a31323a22d0bcd0bed181d0bad0b2d0b0223b733a363a22737472656574223b733a31323a22d0bbd0b5d0bdd0b8d0bdd0b0223b733a363a226e756d626572223b733a313a2231223b7d7d, '2019-10-28 10:29:55', 39000, 0x613a333a7b733a343a226e616d65223b733a363a22536572676579223b733a373a227375726e616d65223b733a363a224976616e6f76223b733a353a22656d61696c223b733a31393a227272726b737372727240676d61696c2e636f6d223b7d, 6);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date` datetime NOT NULL DEFAULT '2010-01-01 10:10:10'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `email`, `password`, `date`) VALUES
(3, 'er', 'wer', 'wer24', '11', '2019-10-11 15:54:29'),
(4, 'Ivan', 'Ivanov', 'ivan@ivan', '111', '2019-10-14 12:05:34'),
(5, 'andrey', 'andreev', 'andrey@andrey', '11', '2019-10-15 11:12:27'),
(6, 'Sergey', 'Ivanov', 'rrrkssrrr@gmail.com', '11', '2019-10-16 09:40:54'),
(7, 'oleg', 'fedorov', 'oleg@oleg', '11', '2019-10-16 11:17:57');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `basket`
--
ALTER TABLE `basket`
  ADD PRIMARY KEY (`id_basket`);

--
-- Индексы таблицы `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id_favorites`);

--
-- Индексы таблицы `goods`
--
ALTER TABLE `goods`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `goods_order`
--
ALTER TABLE `goods_order`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id_order`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;
--
-- AUTO_INCREMENT для таблицы `basket`
--
ALTER TABLE `basket`
  MODIFY `id_basket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id_favorites` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT для таблицы `goods`
--
ALTER TABLE `goods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT для таблицы `goods_order`
--
ALTER TABLE `goods_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
