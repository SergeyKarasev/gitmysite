{*Smarty*}

<footer>
    <div id="footerMain">
        <div id="footContacts">
            <div class="contactWrap">
                <img src="images/envelope.svg" class="contactIcon">
                appliances@appliances.com
            </div>
            <div class="contactWrap">
                <img src="images/phone-call.svg" class="contactIcon">
                555-55-55-55
            </div>
            
        </div>
        <div id="footCategories">
            <a href="index.php?category=hob">ВАРОЧНЫЕ ПОВЕРХНОСТИ</a>
            <a href="index.php?category=oven">ДУХОВЫЕ ШКАФЫ</a>
            <a href="index.php?category=fridge">ХОЛОДИЛЬНИКИ</a>
            <a href="index.php?category=washer">СТИРАЛЬНЫЕ МАШИНЫ</a>
        </div>
        <div id="footNavigation">
            <a href="index.php">ГЛАВНАЯ</a>
            <a href="{$link_basket}">КОРЗИНА</a>
            <a href="{$link_favorites}">ИЗБРАННОЕ</a>
            <a href="{$lk_link_first}">{$lk_title_first}</a>
            <a href="{$lk_link_second}">{$lk_title_second}</a>
        </div>
    </div>
    <div id="footerCopyright">&copy; APPLIANCES</div>
</footer>
</body>
</html>