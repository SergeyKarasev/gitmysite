<?php
if (isset($id_user)) {
	header('Location: index.php?page=profile');
	exit();
}
$title = 'Вход';
$smarty->assign('title', $title);

$data = $_POST;
if (!isset($_GET['r'])) {
	$_GET['r'] = 'index.php?page=profile';
}
$errors = [];
if (isset($data['login'])) {
	//если кнопка была нажата
	$email = $data['email'];
	$password = $data['password'];
	$errors = [];
	if (trim($data['email']) == '') {
		$errors[] = 'Введите Еmail';
	}
	if ($data['password'] == '') {
		$errors[] = 'Введите пароль';
	}	
	if (empty($errors)) {
		//если ошибок нет
		$query = "SELECT * FROM `users` WHERE `email` = '$email';";
		$result = mysqli_query($connection, $query);
		$user = mysqli_fetch_assoc($result);
		mysqli_free_result($result);
		if ($user) { //если пользователь найден
			if ($password == $user['password']) {
				$_SESSION['logged_user'] = $user;
				header('Location: '.$_GET['r']);
				exit();
			} else {
				$errors[] = 'Неверный пароль';
			}
		} else {
			$errors[] = 'Пользователь с таким Email не зарегистрирован';
		}
	}
}
$errors[] = '';
$smarty->assign('errors', $errors);
$smarty->assign('redirect', urlencode($_GET['r']));
if (isset($email)) {
	$smarty->assign('user_email', $email);
} else {
	$smarty->assign('user_email', '');
}

$smarty->display('head.tpl');
$smarty->display('header.tpl');
$smarty->display('login.tpl');
