{*Smarty*}

<div id="categoryFlex">
	{foreach from=$products item=product}
		<div class="shortCard">
			<div class="shortCardImg">
				<img src="{$product.img}"/>
			</div>
			<div class="shortCardName">
				{$product.name}
			</div>
			<div class="shortCardDesc">
				{$product.short_desc}
			</div>
			<div class="shortCardPrice">
				Цена: {$product.price} p.
			</div>
			{if isset($show_button)}
				<button id="fav{$product.id}" class="button" onclick="fav({$product.id})">
					{$change_favorites}
				</button>
				<button id="bask{$product.id}" class="button" onclick="bask({$product.id})">
					{$change_basket}
				</button>
			{/if}
			<a href="index.php?category={$link_category}&id={$product.id}" class="buttonLink">
				Подробнее о товаре
			</a>
		</div>
	{/foreach}				
</div>

<div id="categoryBottom">
	{for $i = 1 to $num_pages+1}
		{if $i == $hash+1}
			<div style="font-weight: bold;">
				<a href="#{$i-1}" onclick="load_product({$i-1}, {$num_goods})">{$i}</a>
			</div>
		{else}
			<a href="#{$i-1}" onclick="load_product({$i-1}, {$num_goods})">{$i}</a>
		{/if}
	{/for}
</div>
{*
{if ($hash == 0) }
		<div>
			<a href="#{$next}" onclick="load_product({$next}, {$num_goods})" class="buttonLink">Дальше</a>
		</div>
{elseif ($hash == $num_pages) }
		<div>
			<a href="#{$prev}" onclick="load_product({$prev}, {$num_goods})" class="buttonLink">Назад</a>
		</div>
{else}
		<div>
			<a href="#{$prev}" onclick="load_product({$prev}, {$num_goods})" class="buttonLink">Назад</a>
			<a href="#{$next}" onclick="load_product({$next}, {$num_goods})" class="buttonLink">Дальше</a>
		</div>
{/if}
*}