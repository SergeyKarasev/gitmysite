<?php
echo phpversion();
if (!isset($id_user)) {
    header('Location: index.php?page=login&r='.urlencode('index.php?page=orders'));
    exit();
}
$title = 'Заказы';
$smarty->assign('title', $title);
$orders = [];
$query = "SELECT * FROM `orders` WHERE `orders`.`id_user` = '$id_user' ORDER BY `orders`.`id_order` DESC;";
if ($result = mysqli_query($connection, $query)) {
	for ($i=0; $orders[$i] = mysqli_fetch_assoc($result) ; $i++) { 
		$address = unserialize($orders[$i]['address']);
		$orders[$i]['address'] = $address;
		$query = "
		SELECT `goods_order`.`name`, `goods_order`.`price`, `goods_order`.`qt` 
		FROM `goods_order`
		WHERE `goods_order`.`id_order` = '".$orders[$i]['id_order']."';";
		if ($result_product = mysqli_query($connection, $query)) {
			while($orders[$i]['products'][] = mysqli_fetch_assoc($result_product));
			array_pop($orders[$i]['products']);
		} else {
			echo "error product";
		}
	}
	array_pop($orders);
	mysqli_free_result($result);
} else {
	echo "error";
}
/*
echo "<pre>";
print_r($orders);
echo "</pre>";
*/
if (empty($orders)) {
	$smarty->assign('empty', 'Вы еще ничего не заказывали.');
} else {
	$smarty->assign('orders', $orders);
}
$smarty->display('head.tpl');
$smarty->display('header.tpl');
$smarty->display('orders.tpl');