{* Smarty *}

<body>
<header>    
    <div id="headerTop">
        <div id="tel">tel. 555-55-55-55</div>
        <div id="email">appliances@appliances.com</div>
        <div id="lk">
            <a href="{$lk_link_first}">{$lk_title_first}</a>
            <a href="{$lk_link_second}">{$lk_title_second}</a>
        </div>
    </div>
    <div id="headerMain">
        <div id="headerCenter">
            <div id="logoCompany">
                <a href="index.php"></a>
            </div>
            <div id="search"></div>
            <div id="favoritesIcon">
                <a href="{$link_favorites}"></a>
            </div>
            <div id="basketIcon">
                <a href="{$link_basket}"></a>
                {*<div class="basketMenu">
                    <div class="basketMenuContent">нет товаров в корзине</div>
                    <a href="{$link_basket}" class="button">Перейти в корзину</a>
                </div>*}
            </div>
        </div>
        <div id="menu">
            <a href="index.php?category=hob">ВАРОЧНЫЕ ПОВЕРХНОСТИ</a>
            <a href="index.php?category=oven">ДУХОВЫЕ ШКАФЫ</a>
            <a href="index.php?category=fridge">ХОЛОДИЛЬНИКИ</a>
            <a href="index.php?category=washer">СТИРАЛЬНЫЕ МАШИНЫ</a>   
        </div>
    </div>
</header>