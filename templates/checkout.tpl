{*Smarty*}

<div id="content">
<div id="contentInside">
	<div class="profileLink">
		<h1>
			<a href="index.php?page=profile">Личный кабинет</a>
			<a href="index.php?page=favorites">Избранное</a>
			<a href="index.php?page=basket">Корзина</a>
			<a href="index.php?page=orders">Заказы</a>
		</h1>
	</div>
	<div class="checkout">
		<div class="editProfile">
			Оформление заказа
		</div>
		<div class="baskFlex">
			<div class="chname">
				Название товара
			</div>
			<div class="baskPrice">
				Цена
			</div>
			<div class="chqt">
				Количество
			</div>
			<div class="baskSum">
				Итого
			</div>
		</div>
		{foreach from=$products item=product}
			<div class="baskFlex">
				<div class="chname">
					{$product.name}	
				</div>
				<div class="baskPrice">
					<span>{$product.price}</span> p.
				</div>
				<div class="chqt">
					<span>{$product.qt}</span> шт.
				</div>
				<div class="baskSum">
					{$product.price * $product.qt} p.
				</div>
			</div>
		{/foreach}
		<div  id="checkoutEnd">
			<div>
				<a href="index.php?page=basket" class="buttonLink">
					Назад в корзину
				</a>
				<form action="index.php?total={$basket_total}&execute=sendcheck" method="POST">
					<div id="bT">
							Всего: <span>{$basket_total}</span> p.					
					</div>
					<div>
						<div>Введите адрес доставки:</div>
						<div>
							<input type="text" name="ad_1_country" required value="{$d_a.country}"     placeholder="Страна">
							<input type="text" name="ad_1_city"    required value="{$d_a.city}"        placeholder="Город">
							<input type="text" name="ad_1_street"  required value="{$d_a.street}"       placeholder="Улица">
							<input type="text" name="ad_1_number"  required value="{$d_a.number}"       placeholder="Номер дома">
						</div>
						<div class="checkbox">
							<input id="check_option" type="checkbox" onclick="test()" name="option" {$checked}> 
							<span>Адрес доставки совпадает с адресом платежа</span>						
						</div>
						<div id="option"></div>
					</div>
					<div class="delAddress">
						<a href="index.php?page=profile&p=edit">выбранный по умолчанию адрес можно изменить в личном кабинете</a>
					</div>
					<input type="submit" class="button" value="Подтвердить адрес и отправить заказ">	
				</form>	
			</div>
		</div>
	</div>
</div>
</div>

