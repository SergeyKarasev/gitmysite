<?php

if (isset($_SESSION['logged_user'])) {
    $smarty->assign(array(
        'lk_title_first' => 'ЛИЧНЫЙ КАБИНЕТ', 
        'lk_link_first' => 'index.php?page=profile',
        'lk_title_second' => 'ВЫЙТИ', 
        'lk_link_second' => 'index.php?execute=logout'
    ));
} else {
    $smarty->assign(array(
        'lk_title_first' => 'ВОЙТИ', 
        'lk_link_first' => 'index.php?page=login',
        'lk_title_second' => 'ЗАРЕГИСТРИРОВАТЬСЯ', 
        'lk_link_second' => 'index.php?page=signup'
    ));
}
$smarty->assign('link_basket', 'index.php?page=basket');
$smarty->assign('link_favorites', 'index.php?page=favorites');