{*Smarty*}

<div id="content">
<div id="contentInside">
	<div class="profileLink">
		<h1>
			<a href="index.php?page=profile">Личный кабинет</a>
			<a href="index.php?page=favorites">Избранное</a>
			<a href="index.php?page=basket">Корзина</a>
			<a href="index.php?page=orders">Заказы</a>
		</h1>
	</div>
		<div class="editProfile">
			Изменение данных профиля
		</div>
		<div class="formFlex">
			<div class="formFlexItem">
				<form action="index.php?page=profile&p=edit" method="POST">
					<div>Введите Имя:</div>
					<input type="text" name="name" required value="{$name}">
					<div>Введите Фамилию:</div>
					<input type="text" name="surname" required value="{$surname}">
					<div>Введите E-mail:</div>
					<input type="text" 	name="email" required value="{$email}">
					<div>
						<input class="button" type="submit" name="edit_data" value="Сохранить">
					</div>
				</form>
			</div>
			<div class="formFlexItem">
				<form action="index.php?page=profile&p=edit" method="POST">
					<div>Введите Пароль:</div>
					<input type="password" 	name="password" required>
					<div>Повторите Пароль:</div>
					<input type="password" 	name="password1" required>
					<div>
						<input class="button" type="submit" name="edit_password" value="Изменить пароль">
					</div>
				</form>
			</div>
		</div>
			<div class="formAddress">
				<form action="index.php?page=profile&p=edit" method="POST">
					<div>Введите адрес доставки:</div>
					<input type="text" name="ad_1_country" required value="{$d_a.country}"     placeholder="Страна">
					<input type="text" name="ad_1_city"    required value="{$d_a.city}"        placeholder="Город">
					<input type="text" name="ad_1_street"  required value="{$d_a.street}"       placeholder="Улица">
					<input type="text" name="ad_1_number"  required value="{$d_a.number}"       placeholder="Номер дома">
					<div class="checkbox">
						<input id="check_option" type="checkbox" onclick="test()" name="option" {$checked}> 
						<span>Адрес доставки совпадает с адресом платежа</span>						
					</div>
					<div id="option">
					</div>
					<div>
						<input class="button" type="submit" name="edit_address" value="Сохранить">
					</div>
				</form>
			</div>
		<div class="formErrors">{$error}</div>
</div>
</div>

