<?php
if (!isset($id_user)) {
	header('Location: index.php?page=login');
}
$title = 'Мой профиль';
$smarty->assign('checked', 'checked');
$addresses = get_addresses($id_user, $connection);
if ($addresses) {
	$smarty->assign(array(
		'd_a' => $addresses['type_1'],
		'p_a' => $addresses['type_2']
	));
	if ($addresses['type_1'] != $addresses['type_2']) {
		$smarty->assign('checked', '');
	}
} else {
	$smarty->assign('no_data', 'Нет данных');

}
$smarty->assign(array(
	'name' => $_SESSION['logged_user']['name'],
	'surname' => $_SESSION['logged_user']['surname'],
	'email' => $_SESSION['logged_user']['email']
));
$edit_view = FALSE;
if (isset($_GET['p'])) {
	if ($_GET['p'] == 'edit') {
		$edit_view = TRUE;
		$title = 'Настройки';
		$notice = [];
		// name email
		if (isset($_POST['edit_data'])) {
			$name = $_POST['name'];
			$surname = $_POST['surname'];
			$email = $_POST['email'];
			$search = mysqli_query($connection, "SELECT * FROM `users` WHERE `email` = '$email' AND `id` <> '$id_user';");
			if (mysqli_num_rows($search) != 0) {
				$notice[] = 'Этот E-mail зарегистрирован другим пользователем.';
			}
			if (empty($notice)) {
				$query = "UPDATE `users` SET `name` = '$name', `surname` = '$surname', `email` = '$email' WHERE `users`.`id` = '$id_user';";
				if (mysqli_query($connection, $query)) {
					$result = mysqli_query($connection, "SELECT * FROM `users` WHERE `email` = '$email';");
					$user = mysqli_fetch_assoc($result);
					mysqli_free_result($result);
					$_SESSION['logged_user'] = $user;
					$notice[] = 'Данные были изменены';
					$smarty->assign(array(
						'name' => $_SESSION['logged_user']['name'],
						'surname' => $_SESSION['logged_user']['surname'],
						'email' => $_SESSION['logged_user']['email']
					));
				} else {
					$notice[] = 'Данные не изменены';
				}
			}
		}
		//password
		if (isset($_POST['edit_password']))	{
			$password = $_POST['password'];
			if ($_POST['password1'] != $_POST['password']) {
				$notice[] = 'Пароли отличаются. Данные не изменены';
			}
			if (empty($notice)) {
				$query = "UPDATE `users` SET `password` = '$password' WHERE `users`.`id` = '$id_user';";
				if (mysqli_query($connection, $query)) {
					$notice[] = 'Данные были изменены';
				} else {
					$notice[] = 'Данные не изменены';
				}
			}
		}
		//addresses
		if (!$addresses) {
			$addresses = [];
			$addresses['type_1']['country'] = '';
			$addresses['type_1']['city'] = '';
			$addresses['type_1']['street'] = '';
			$addresses['type_1']['number'] = '';
			$addresses['type_2'] = $addresses['type_1'];
		}
		$smarty->assign(array(
			'd_a' => $addresses['type_1'],
			'p_a' => $addresses['type_2']
		));
		if (isset($_POST['edit_address'])) {
			$query = "DELETE FROM `addresses` WHERE `addresses`.`id_user` = '$id_user';";
			mysqli_query($connection, $query);
			
			$addresses['type_1']['country'] = $_POST['ad_1_country'];
			$addresses['type_1']['city'] = $_POST['ad_1_city'];
			$addresses['type_1']['street'] = $_POST['ad_1_street'];
			$addresses['type_1']['number'] = $_POST['ad_1_number'];
			$smarty->assign('d_a', $addresses['type_1']);
			if (!isset($_POST['option'])) {
				$addresses['type_2']['country'] = $_POST['ad_2_country'];
				$addresses['type_2']['city'] = $_POST['ad_2_city'];
				$addresses['type_2']['street'] = $_POST['ad_2_street'];
				$addresses['type_2']['number'] = $_POST['ad_2_number'];
				$notice = insert_address($id_user, $connection, $addresses['type_1'], $addresses['type_2']);				
				$smarty->assign('p_a', $addresses['type_2']);
				$smarty->assign('checked', '');
			} else {
				$notice = insert_address($id_user, $connection, $addresses['type_1'], $addresses['type_1']);
				$smarty->assign('p_a', $addresses['type_1']);
				$smarty->assign('checked', 'checked');
			}
		}
		$notice[] = '';
		$smarty->assign('error', $notice[0]);
	}
}

$smarty->assign('title', $title);
$smarty->display('head.tpl');
$smarty->display('header.tpl');
$profile_view = !$edit_view;
if ($edit_view) {
	$smarty->display('edit_profile.tpl');
}
if ($profile_view) {
	$smarty->display('profile.tpl');
}
