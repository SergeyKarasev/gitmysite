{*Smarty*}

<div id="content">
	<div id="contentInside">
		<div class="breadCrumbs">
			<a href="index.php">Главная</a>
		</div>
		<h1>
			Вход
		</h1>
		<div>
			<form action="index.php?page=login&r={$redirect}" method="POST" id="form">
				<div class="formFlex">
					<div class="formFlexItem">
						<div>Введите E-mail:</div>
						<input type="text" 	name="email" required value="{$user_email}">
					</div>
					<div class="formFlexItem">
						<div>Введите Пароль:</div>
						<input type="password" 	name="password" required>
						<br>
					</div>
				</div>
				<input class="button" type="submit" name="login" value="Войти">
			</form>
			<div class="formErrors">{$errors[0]}</div>
		</div>
	</div>
</div>