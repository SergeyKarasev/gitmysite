{*Smarty*}

<div id="content">
<div id="contentInside">
	<div class="profileLink">
		<h1>
			<span>Личный кабинет</span>
			<a href="index.php?page=favorites">Избранное</a>
			<a href="index.php?page=basket">Корзина</a>
			<a href="index.php?page=orders">Заказы</a>
		</h1>
	</div>
	<div class="profileData">
		<div>{$name} {$surname} <a href="index.php?page=profile&p=edit">Изменить</a></div>
		<div>Адрес электронной почты: {$email} </div>
		{if isset($no_data)}
			Адрес доставки: {$no_data}
		{else}
			<div>
				Адрес доставки: 
				Страна {$d_a.country}, 
				город {$d_a.city},
				улица {$d_a.street}, 
				дом №{$d_a.number},
			</div>
			<div>
				Адрес платежа: 
				Страна {$p_a.country},
				город {$p_a.city}, 
				улица {$p_a.street}, 
				дом №{$p_a.number},
			</div>
		{/if}
	</div>
</div>
</div>

