{*Smarty*}

<div id="content">
<div id="contentInside">
	<div class="profileLink">
		<h1>
			<a href="index.php?page=profile">Личный кабинет</a>
			<a href="index.php?page=favorites">Избранное</a>
			<span>Корзина</span>
			<a href="index.php?page=orders">Заказы</a>
		</h1>
	</div>
	<div class="basketList">
		{if isset($empty)}
			{$empty}
		{else}
			{foreach from=$products item=product}
				<div class="good{$product.id_good}">
					<div class="baskFlex">
						<div class="baskImg">
							<a href="index.php?category={$product.catid}&id={$product.id_good}">
								<img src="{$product.img}"/>
							</a>
						</div>
						<div class="baskName">
							<a href="index.php?category={$product.catid}&id={$product.id_good}">
								{$product.name}
							</a>					
						</div>
						<div class="baskPrice">
							Цена: <span id="priceId{$product.id_good}">{$product.price}</span> p.
						</div>
						<div class="baskQt unselectable">
							<span class="calc unselectable" onclick="calcqt({$product.id_good}, -1)">
							-</span>
							<span class="qt" id="qt{$product.id_good}" >{$product.qt}</span>
							<span class="calc unselectable" onclick="calcqt({$product.id_good}, 1)">
							+</span>
						</div>
						<div class="baskSum" id="baskSumId{$product.id_good}">
							{$product.price * $product.qt} p.
						</div>
						<div class="unselectable baskDel">
							<span onclick="del_item({$product.id_good}, 'bask')">&#10006;</span>
						</div>
					</div>
				</div>
			{/foreach}
			<div id="basketEnd">
				<div>
					<input class="button clean_basket" type="button" value="Очистить корзину">
				</div>
				<div>
					<div id="bT">
						Всего: 
						<span id="basketTotal">{$basket_total}</span>
						 p.
					</div>
					<a href="index.php?page=basket&p=checkout" class="buttonLink">
						Оформить заказ
					</a>
				</div>
			</div>
		{/if}
	</div>
</div>
</div>

