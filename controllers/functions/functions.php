<?php 
function check_good_status($id_user, $id_good, $connection) {
	$query = "
	SELECT `id`, `id_basket`, `id_favorites` 
	FROM `goods` 
	LEFT JOIN `basket` ON `basket`.`id_good` = `goods`.`id` AND `basket`.`id_user` = '$id_user' 
	LEFT JOIN `favorites` ON `favorites`.`id_good` = `goods`.`id` AND `favorites`.`id_user` = '$id_user' 
	WHERE `goods`.`id` = '$id_good';";
    $result = mysqli_query($connection, $query);
    $check = mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    return $check;
}

function get_addresses($id_user, $connection) {
	$addresses = [];
	$query = "SELECT `address` FROM `addresses` WHERE `id_user` = '$id_user' AND `type` = '1';";
	$result = mysqli_query($connection, $query);
	$address = mysqli_fetch_assoc($result);
	mysqli_free_result($result);
	$addresses['type_1'] = unserialize($address['address']);
	if (isset($addresses['type_1']['country'])) {
		$query = "SELECT `address` FROM `addresses` WHERE `id_user` = '$id_user' AND `type` = '2';";
		$result = mysqli_query($connection, $query);
		$address = mysqli_fetch_assoc($result);
		mysqli_free_result($result);
		$addresses['type_2'] = unserialize($address['address']);
	} else {
		$addresses = FALSE;
	}
	return $addresses;
}

function insert_address($id_user, $connection, $ad1, $ad2) {
	$notice = [];
	$address = serialize($ad1);
	$query = "INSERT INTO `addresses` (`id`, `id_user`, `address`, `type`) VALUES (NULL, '$id_user', '$address', '1');";
	if (mysqli_query($connection, $query)) {
		$notice[] = 'Данные были изменены';
	} else {
		$notice[] = 'Данные не изменены3';
	}

	$address = serialize($ad2);
	$query = "INSERT INTO `addresses` (`id`, `id_user`, `address`, `type`) VALUES (NULL, '$id_user', '$address', '2');";
	if (mysqli_query($connection, $query)) {
		$notice[] = 'Данные были изменены';
	} else {
		$notice[] = 'Данные не изменены4';
	}
	return $notice;
}

function get_message($basket_total, $id_order, $date, $addresses, $connection) {
	$message = 'Здравствуйте, ';
	$message .= $_SESSION['logged_user']['name'] . ' ';
	$message .= $_SESSION['logged_user']['surname'] . '.<br>';
	$message .= 'Ваш заказ №' . $id_order . ' принят в обработку!<br>';
	$message .= 'Дата заказа: '.substr($date, 0, 10).'<br>';
	$message .= 'Адрес доставки: ';
	$message .= 'страна ' . $addresses['type_1']['country'] . ', ';
	$message .= 'город ' . $addresses['type_1']['city'] . ', ';
	$message .= 'улица ' . $addresses['type_1']['street'] . ', ';
	$message .= 'номер дома ' . $addresses['type_1']['number'] . '.<br>';
	$check = array_diff($addresses['type_1'], $addresses['type_2']);
	if (!empty($check)) {
		$message .= 'Адрес платежа: ';
		$message .= 'страна ' . $addresses['type_2']['country'] . ', ';
		$message .= 'город ' . $addresses['type_2']['city'] . ', ';
		$message .= 'улица ' . $addresses['type_2']['street'] . ', ';
		$message .= 'д. ' . $addresses['type_2']['number'] . '.<br>';
	}
	$message .= '<br>В Вашем заказе: <br>';
	$query = "SELECT * FROM `goods_order` WHERE `goods_order`.`id_order` = '$id_order';";
	if ($result = mysqli_query($connection, $query)) {
		while ($product = mysqli_fetch_assoc($result)) {
			if (!empty($product)) {
				$message .= $product['name'] . ' ';
				$message .= $product['qt'] . 'шт ';
				$message .= 'по ' . $product['price'] . 'р.<br>';		
			}
		}
	}
	$message .= 'Общая сумма заказа: ' . $basket_total . 'p.<br>';
	$message .= '<br>Спасибо за заказ!<br>';
	$message .= '<br>Это письо было создано автоматически и не требует ответа.<br>';
	return $message;
}