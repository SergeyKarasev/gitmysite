{*Smarty*}

<div id="content">
<div id="contentInside">
	<div class="breadCrumbs">
		<a href="index.php">Главная</a>
	</div>
	<div class="profileLink">
		<h1>
			<a href="index.php?page=profile">Личный кабинет</a>
			<a href="index.php?page=favorites">Избранное</a>
			<a href="index.php?page=basket">Корзина</a>
			<span>Заказы</span>
		</h1>
	</div>
	<div class="ordersList">
		{if isset($empty)}
			{$empty}
		{else}
			{foreach from=$orders item=order}
				<div class="orderItem">	
					<div class="headFlex">
						<span>
							Заказ №{$order.id_order}
						</span>
						<span>
							Дата заказа: {$order.date}
						</span>
						<span>
							Сумма заказа: {$order.total}p.
						</span>
					</div>
					<div class="headFlex">
						<span>
							Адрес: страна {$order.address.type_1.country}
							, город {$order.address.type_1.city}
							, улица {$order.address.type_1.street}
							, дом {$order.address.type_1.number}
						</span>
					</div>
					{foreach from=$order.products item=product}
						<div class="productItem">
							<span class="grow">
								{$product.name}
							</span>
							<div class="price">
								<span>
									Цена: {$product.price}р. 
								</span>
								<span>
									Кол-во: {$product.qt}шт.
								</span>
								<span>
									Сумма: {$product.qt * $product.price}р.
								</span>		
							</div>
						</div>
					{/foreach}
				</div>			
			{/foreach}
		{/if}
	</div>
</div>
</div>

