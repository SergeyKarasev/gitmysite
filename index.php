<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

session_start();
if (isset($_SESSION['logged_user'])) {
    $id_user = $_SESSION['logged_user']['id'];
}

require_once 'libs/Smarty.class.php';
$smarty = new Smarty();

require_once 'controllers/db.php';
require_once 'controllers/functions/functions.php';
require_once 'controllers/header.php';
require_once 'controllers/functions/mailer/SendMailSmtpClass.php';

if (isset($_GET['execute'])) {
    switch ($_GET['execute']) {
        case 'change_menu':
            require 'controllers/change_menu.php';
            break;

        case 'change_status':
            require 'controllers/change_status.php';
            break;

        case 'load_product':
            require 'controllers/load_product.php';
            break;

        case 'sendcheck':
            require 'controllers/send_checkout.php';
            break;

        case 'qt':
            require 'controllers/qt_basket.php';
            break;

        case 'pa':
            require 'controllers/payment_address.php';
            break;

        case 'clean_basket':
            require 'controllers/clean_basket.php';
            break;

        case 'logout':
            require 'controllers/logout.php';
            break;
    }
} else {
    if (isset($_GET['page'])) {
        switch ($_GET['page']) {
            case 'basket':
                require 'controllers/basket.php';
                break;
            
            case 'profile':
                require 'controllers/profile.php';
                break;
            
            case 'favorites':
                require 'controllers/favorites.php';
                break;
            
            case 'orders':
                require 'controllers/orders.php';
                break;

            case 'login':
                require 'controllers/login.php';
                break;

            case 'signup':
                require 'controllers/signup.php';
                break;
            
            default:
                require 'controllers/content.php';
                break;
        }
    } else {
        require 'controllers/content.php';
    }
        

    $smarty->display('footer.tpl');
}
mysqli_close($connection);