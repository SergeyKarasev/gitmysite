<?php

if (isset($_GET['category'])) {
    //если в category чтото попало
    $category = $_GET['category'];
    switch ($category) {
        case 'hob':
            $categoryName = 'Варочные поверхности';
            break;
        case 'fridge':
            $categoryName = 'Холодильники';
            break;
        case 'oven':
            $categoryName = 'Духовые шкафы';
            break;
        case 'washer':
            $categoryName = 'Стиральные машины';
            break;
    }
    if (isset($categoryName)) {
        //если category корректен
        $smarty->assign('link_category', $category);
        $smarty->assign('category_name', $categoryName);
        if (isset($_GET['id'])) {
            //если в id чтото попало то ищем товар по id catid
            $id = $_GET['id'];
            $query = "SELECT * FROM `goods` WHERE `id` = '$id' AND `catid` = '$category';";
            $result = mysqli_query($connection, $query);
            $good = mysqli_fetch_assoc($result);
            mysqli_free_result($result);
        } 
        if (isset($good)) {
            //если товар найден то выводим
            $smarty->assign('good', $good);
            if (isset($id_user)) {
                //если пользователь авторизован то добавляем кнопки
                $check = check_good_status($id_user, $id, $connection);
                if (isset($check['id_favorites'])) {
                    $smarty->assign('change_favorites', 'Убрать из избранного');
                } else {
                    $smarty->assign('change_favorites', 'Добавить в избранное');
                }
                if (isset($check['id_basket'])) {
                    $smarty->assign('change_basket', 'Убрать из корзины');
                } else {
                    $smarty->assign('change_basket', 'Добавить в корзину');
                }
                $smarty->assign('show_button', 'show');
            }
            $smarty->assign('title', $good['name']);
            $smarty->display('head.tpl');
            $smarty->display('header.tpl');
            $smarty->display('product.tpl');
        } else {
            //если товар не нашли или не искали то выводим всю категорию
            $query = "SELECT COUNT(*) FROM `goods` WHERE `goods`.`catid` = '$category';";
            if ($result = mysqli_query($connection, $query)) {
                $count = mysqli_fetch_assoc($result);
            }
            if (isset($count)) {
                $smarty->assign('count', $count['COUNT(*)']);
            } else {
                $smarty->assign('count', 0);
            }

            if (isset($_SESSION['num_goods'])) {
                $smarty->assign('num_goods', $_SESSION['num_goods']);
                switch ($_SESSION['num_goods']) {
                    case 3:
                        $smarty->assign('num_goods', 3);
                        $smarty->assign('num_goods_1', 6);
                        $smarty->assign('num_goods_2', 9);         
                        break;
                    case 6:
                        $smarty->assign('num_goods', 6);
                        $smarty->assign('num_goods_1', 3);
                        $smarty->assign('num_goods_2', 9);         
                        break;
                    case 9:
                        $smarty->assign('num_goods', 9);
                        $smarty->assign('num_goods_1', 3);
                        $smarty->assign('num_goods_2', 6);         
                        break;
                 } 
            } else {
                $smarty->assign('num_goods', 3);
                $smarty->assign('num_goods_1', 6);
                $smarty->assign('num_goods_2', 9);
            }
            
            $smarty->assign('title', $categoryName);
            $smarty->display('head.tpl');
            $smarty->display('header.tpl');
            $smarty->display('category.tpl');
            //содержимое категории отрисовывается через ajax
        }
    } else {
        //если category не корректен
        $smarty->assign('title', 'Home appliances');
        $smarty->display('head.tpl');
        $smarty->display('header.tpl');
        $smarty->display('main.tpl');
    }
} else {
    //если в category пусто
    $smarty->assign('title', 'Home appliances');
    $smarty->display('head.tpl');
    $smarty->display('header.tpl');
    $smarty->display('main.tpl');
}