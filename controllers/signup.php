<?php
if (isset($id_user)) {
	header('Location: index.php?page=profile');
	exit();
}
$title = 'Регистрация';
$smarty->assign('title', $title);
$data = $_POST;
if (isset($data['signup'])) {
	//если кнопка была нажата
	$name = $data['name'];
	$surname = $data['surname'];
	$email = $data['email'];
	$password = $data['password'];
	$date = date('Y-m-d H:i:s');

	$errors = [];
	if ($data['password1'] != $data['password']) {
		$errors[] = 'Введите пароль еще раз. Пароли отличаются';
	}
	$search = mysqli_query($connection, "SELECT * FROM `users` WHERE `email` = '$email';");
	if (mysqli_num_rows($search) != 0) {
		$errors[] = 'Этот E-mail уже зарегистрирован. Попробуйте войти';
	}
	if (empty($errors)) {
		//если ошибок нет
		$query = "INSERT INTO `users` (`id`, `name`, `surname`, `email`, `password`, `date`) VALUES (NULL, '$name', '$surname', '$email', '$password', '$date');";
		if (mysqli_query($connection, $query)) {
			//если запись была совершена
			$result = mysqli_query($connection, "SELECT * FROM `users` WHERE `email` = '$email';");
			$_SESSION['logged_user'] = mysqli_fetch_assoc($result);
			mysqli_free_result($result);
			header('Location: index.php?page=profile');
			exit();
		} else {
			//запись в бд завершилась ошибкой
			$errors[] = 'Пользователь не был зарегистрирован';
		}
	}
}
$errors[] = '';
$smarty->assign('errors', $errors);
if (isset($name, $surname, $email)) {
	$smarty->assign(array(
        'user_name' => $name, 
        'user_surname' => $surname,
        'user_email' => $email
    ));
} else {
	$smarty->assign(array(
        'user_name' => '', 
        'user_surname' => '',
        'user_email' => ''
    ));
}

$smarty->display('head.tpl');
$smarty->display('header.tpl');
$smarty->display('signup.tpl');
