<?php
if (!isset($id_user)) {
	header('Location: index.php?page=login');
	exit();
}
if (!isset($_GET['total'])) {
	header('Location: index.php?page=basket');
	exit();
}
$basket_total = $_GET['total'];
if (!isset($_POST['ad_1_country'], $_POST['ad_1_city'], $_POST['ad_1_street'], $_POST['ad_1_number'])) {
	header('Location: index.php?page=basket&p=checkout');
	exit();
}
$addresses['type_1']['country'] = $_POST['ad_1_country'];
$addresses['type_1']['city'] = $_POST['ad_1_city'];
$addresses['type_1']['street'] = $_POST['ad_1_street'];
$addresses['type_1']['number'] = $_POST['ad_1_number'];
if (isset($_POST['option'])) {
	$addresses['type_2'] = $addresses['type_1'];
} else {
	if (!isset($_POST['ad_2_country'], $_POST['ad_2_city'], $_POST['ad_2_street'], $_POST['ad_2_number'])) {
		header('Location: index.php?page=basket&p=checkout');
		exit();
	}
	$addresses['type_2']['country'] = $_POST['ad_2_country'];
	$addresses['type_2']['city'] = $_POST['ad_2_city'];
	$addresses['type_2']['street'] = $_POST['ad_2_street'];
	$addresses['type_2']['number'] = $_POST['ad_2_number'];
}
$address = serialize($addresses);
$date = date('Y-m-d H:i:s');
$user_data = [];
$user_data['name'] = $_SESSION['logged_user']['name'];
$user_data['surname'] = $_SESSION['logged_user']['surname'];
$user_data['email'] = $_SESSION['logged_user']['email'];
$customer_data = serialize($user_data);
echo "<pre>";
print_r($addresses);
print_r($user_data);
echo "</pre>";

$query = "INSERT INTO `orders` VALUES (NULL, '$address', '$date', '$basket_total', '$customer_data', '$id_user');";
if (mysqli_query($connection, $query)) {
	$id_order = mysqli_insert_id($connection);
	$query = "INSERT INTO `goods_order` (`id`, `id_order`, `id_user`, `id_good`, `qt`, `price`, `name`)	
	SELECT
	  NULL,
	  '$id_order',
	  `basket`.`id_user`,
	  `basket`.`id_good`,
	  `basket`.`qt`,
	  `goods`.`price`,
	  `goods`.`name`
	FROM
	  `basket`
	    LEFT JOIN
	      `goods`
	    ON
	      `basket`.`id_good` = `goods`.`id`
	WHERE
	  `basket`.`id_user` = '$id_user'
	;";
	if (mysqli_query($connection, $query)) {
		/*send email*/
		$mailSMTP = new SendMailSmtpClass('test.home.appliances.test@gmail.com', 'zaq1XSW2', 'ssl://smtp.gmail.com', 465, "UTF-8");
		$from = array(
			"HomeAppliances", // Имя отправителя
			"test.home.appliances.test@gmail.com" // почта отправителя
		);
		$to = $user_data['email'];
		$message = get_message($basket_total, $id_order, $date, $addresses, $connection);
		$result =  $mailSMTP->send($to, 'Новый заказ', $message, $from); 
		if($result === true){
			echo "Done<br>";
		}else{
			echo "Error: ".$result."<br>";
		}
		/*end send email*/
		$query = "DELETE FROM `basket` WHERE `basket`.`id_user` = '$id_user';";
		if (mysqli_query($connection, $query)) {
			echo "clean basket<br>";	
		}
		header('Location: index.php?page=orders');
		exit();
	} else {
		echo "error writing to goods for orders<br>";
		$query = "DELETE FROM `orders` WHERE `orders`.`id` = '$id_order';";
		if (mysqli_query($connection, $query)) {
			echo "delete order<br>";	
		}	
	}
} else {
	echo "error writing to orders<br>";
}