{* Smarty *}

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet/less" type="text/css" href="styles/style-block.less" />
    <link rel="stylesheet/less" type="text/css" href="styles/style-header.less" />
    <link rel="stylesheet/less" type="text/css" href="styles/style-pages.less" />
    <link rel="stylesheet/less" type="text/css" href="styles/style-contentpages.less" />
    <link rel="stylesheet/less" type="text/css" href="styles/style-authpages.less" />
    <link rel="stylesheet/less" type="text/css" href="styles/style-profilepages.less" />
    <link rel="stylesheet/less" type="text/css" href="styles/style-footer.less" />

	<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.9.0/less.min.js" ></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
	<script src="scripts/scripts.js"></script>
    <title>{$title}</title>
</head>