{*Smarty*}

<div id="content">
	<div id="contentInside">
		<div class="breadCrumbs">
			<a href="index.php">Главная</a>
		</div>
		<h1>
			{$category_name}
		</h1>
		<div class="categoryTop">
			<div>
				Bсего {$count} товаров в выбранной категории.	
			</div>
			<div id="numGoods">
				Выводить по {$num_goods} шт
				<div class="numGoodsMenu">
					<a href="#0" onclick="load_product(0, {$num_goods_1})" >
						Выводить по {$num_goods_1} шт
					</a>
					<a href="#0" onclick="load_product(0, {$num_goods_2})" >
						Выводить по {$num_goods_2} шт
					</a>
				</div>
			</div>
		</div>
		<div id="ajaxContent">
			{*данные выгружаются в этот контейнер с помощью ajax*}
		</div>
	</div>
</div>

