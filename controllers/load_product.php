<?php
$category = $_GET['category'];
$query = "SELECT * FROM `goods` WHERE `goods`.`catid` = '$category';";
$result = mysqli_query($connection, $query);
$num_rows = mysqli_num_rows($result);
mysqli_free_result($result);

if (isset($_GET['num_goods'])) {
	$num_goods = $_GET['num_goods'];
} elseif (isset($_SESSION['num_goods'])) {
	$num_goods = $_SESSION['num_goods'];
} else {
	$num_goods = 3;	
}
$hash = $_GET['hash'];
if ($hash <= 0) {
	$hash = 0;
}
$num_pages = ceil($num_rows / $num_goods) - 1;
if ($hash > $num_pages) {
	$hash = $num_pages;
}
$num = $hash * $num_goods;
$products = [];
$query = "SELECT * FROM `goods` WHERE `goods`.`catid` = '$category' LIMIT $num, $num_goods;";
if ($result = mysqli_query($connection, $query)) {
	while ($products[] = mysqli_fetch_assoc($result));
	array_pop($products);
	mysqli_free_result($result);
} else {
	echo "error";
}

$prev = $hash - 1;
$next = $hash + 1;
$smarty->assign(array(
	'num_goods' => $num_goods,
	'products' => $products,
	'link_category' => $category,
	'num_pages' => $num_pages,
	'hash' => $hash,
	'prev' => $prev,
	'next' => $next
));
$smarty->display('loaded_products.tpl');