<?php
if (!isset($id_user)) {
	header('Location: index.php?page=login&r='.urlencode('index.php?page=basket'));
	exit();
}
$title = 'Корзина';
$smarty->assign('title', $title);
$basket_this_user = [];
$query = "SELECT `basket`.`id_good`, `basket`.`qt`, `goods`.`price`, `goods`.`catid`, `goods`.`name`, `goods`.`img` FROM `basket` LEFT JOIN `goods` ON `basket`.`id_good` = `goods`.`id` WHERE `basket`.`id_user` = '$id_user';";
$result = mysqli_query($connection, $query);
$basket_total = 0;
for ($i=0; $basket_this_user[] = mysqli_fetch_assoc($result) ; $i++) { 
	$basket_total += ($basket_this_user[$i]['qt'] * $basket_this_user[$i]['price']);
}
mysqli_free_result($result);
array_pop($basket_this_user);
if (empty($basket_this_user)) {
	$smarty->assign('empty', 'Ваша корзина пуста.');
} else {
	$smarty->assign('basket_total', $basket_total);
	$smarty->assign('products', $basket_this_user);
}
$checkout_view = FALSE;
if (isset($_GET['p'])) {
	if ($_GET['p'] == 'checkout') {
		if (!empty($basket_this_user)) {
			$checkout_view = TRUE;
			$addresses = get_addresses($id_user, $connection);
			$smarty->assign('checked', 'checked');
			if ($addresses['type_1'] != $addresses['type_2']) {
				$smarty->assign('checked', '');
			}
			$smarty->assign(array(
				'd_a' => $addresses['type_1'],
				'p_a' => $addresses['type_2']
			));
		}
	}
}
$basket_view = !$checkout_view;
$smarty->display('head.tpl');
$smarty->display('header.tpl');
if ($checkout_view) {
	$smarty->display('checkout.tpl');
}
if ($basket_view) {
	$smarty->display('basket.tpl');
}
/*
echo "<pre>";
print_r($basket_this_user);
echo "</pre>";
*/