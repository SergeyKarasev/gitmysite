{*Smarty*}

<div id="content">
<div id="contentInside">
	<div class="profileLink">
		<h1>
			<a href="index.php?page=profile">Личный кабинет</a>
			<span>Избранное</span>
			<a href="index.php?page=basket">Корзина</a>
			<a href="index.php?page=orders">Заказы</a>
		</h1>
	</div>
	<div class="favoritesList">
		{if isset($empty)}
			{$empty}
		{else}
			{foreach from=$products item=product}
				<div class="good{$product.id}">
					<div class="favFlex">
						<div class="favImg">
							<a href="index.php?category={$product.catid}&id={$product.id}">
								<img src="{$product.img}"/>
							</a>
						</div>
						<div class="favDesc">
							<div>
								<a href="index.php?category={$product.catid}&id={$product.id}">
									{$product.name}
								</a>
							</div>
							<div>
								{$product.short_desc}
							</div>
							<div>
								Цена: {$product.price} p.
							</div>
						</div>
						<div class="favBut">	
							<button id="fav{$product.id}" class="button" onclick="del_item({$product.id}, 'fav')">
								Убрать из избранного
							</button>
							<button id="bask{$product.id}" class="button" onclick="change({$product.id}, 'bask')">
								{$product.change_basket}
							</button>
							<a href="index.php?category={$product.catid}&id={$product.id}" class="buttonLink">
								Подробнее о товаре
							</a>
						</div>
					</div>
				</div>
			{/foreach}
		{/if}
	</div>
</div>
</div>

