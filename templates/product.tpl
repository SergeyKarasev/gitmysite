{*Smarty*}

<div id="content">
<div id="contentInside">
	<div class="breadCrumbs">
		<a href="index.php">Главная</a>
		-
		<a href="index.php?category={$link_category}">{$category_name}</a>
	</div>
	<div class="product">
		<div class="productImg">
			<img src="{$good.img}"/>
		</div>
		<div>    
			<div id="productName">
				{$good.name}
			</div>
			<div id="productDesc">
				{$good.description}
			</div>
			<div id="productPrice">
				{$good.price} p.
			</div>
			{if isset($show_button)}
				<button id="fav{$good.id}" class="button" onclick="change({$good.id}, 'fav')">
					{$change_favorites}
				</button>
				<button id="bask{$good.id}" class="button" onclick="change({$good.id}, 'bask')">
					{$change_basket}
				</button>
			{/if}
			<input type="button" onclick="history.back();" value="Назад" class="button">
		</div>
	</div>
</div>
</div>
