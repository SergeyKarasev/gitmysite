<?php
if (!isset($id_user)) {
  header('Location: index.php?page=login&r='.urlencode('index.php?page=favorites'));
  exit();
}
$title = 'Избранное';
$smarty->assign('title', $title);
$favorites_this_user = [];
$query = "SELECT * FROM `favorites` LEFT JOIN `goods` ON `favorites`.`id_good` = `goods`.`id` WHERE `favorites`.`id_user` = '$id_user';";
$result = mysqli_query($connection, $query);
for ($i = 0; $favorites_this_user[$i] = mysqli_fetch_assoc($result); $i++) {
  $check = check_good_status($id_user, $favorites_this_user[$i]['id'], $connection);
  if (isset($check['id_basket'])) {
      $favorites_this_user[$i]['change_basket'] = 'Убрать из корзины';
  } else {
      $favorites_this_user[$i]['change_basket'] = 'Добавить в корзину';
  }
}
mysqli_free_result($result);
array_pop($favorites_this_user);
if (empty($favorites_this_user)) {
  $smarty->assign('empty', 'Пусто. Нет избранных товаров. Добавьте товары из каталога.');
}
$smarty->assign('products', $favorites_this_user);
$smarty->display('head.tpl');
$smarty->display('header.tpl');
$smarty->display('favorites.tpl');
/*
echo "<pre>";
print_r($favorites_this_user);
echo "</pre>";
*/