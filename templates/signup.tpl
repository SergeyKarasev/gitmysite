{*Smarty*}

<div id="content">
	<div id="contentInside">
		<div class="breadCrumbs">
			<a href="index.php">Главная</a>
		</div>
		<h1>
			Регистрация
		</h1>
		<div>
			<form action="index.php?page=signup" method="POST" id="form">
				<div class="formFlex">
					<div class="formFlexItem">
						<div>Введите Имя:</div>
						<input type="text" 		name="name" 	required value="{$user_name}">
						<div>Введите Фамилию:</div>
						<input type="text" 		name="surname" 	required value="{$user_surname}">
						<div>Введите E-mail:</div>
						<input type="text" 	name="email" 	required value="{$user_email}">
					</div>
					<div class="formFlexItem">
						<div>Введите Пароль:</div>
						<input type="password" 	name="password" required>
						<div>Повторите Пароль:</div>
						<input type="password" 	name="password1" required>
						<br>
						
					</div>
				</div>
				<input class="button" type="submit" name="signup" value="Зарегистрироваться">
			</form>
			<div class="formErrors">{$errors[0]}</div>
		</div>
	</div>
</div>