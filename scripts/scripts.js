$(function(){
   var hash = window.location.hash.slice(1);
   var category = window.location.search.slice(10);
   $.ajax({
      url:"index.php",
      data:"hash="+hash+"&execute=load_product&category="+category,
      success:function(result){
         $('#ajaxContent').html(result);
      }
   });
});

function load_product(hash, num) {
   var category = window.location.search.slice(10);
   $.ajax({
      url:"index.php",
      data:"hash="+hash+"&num_goods="+num+"&execute=load_product&category="+category,
      success:function(result){
         $('#ajaxContent').html(result);
         $("#numGoods").load("index.php", 'execute=change_menu&num_goods='+num);
      }
   });
}

$(document).ready(function(){
   $(".clean_basket").click(function(){
         $(".basketList").load("index.php", 'execute=clean_basket');
   })

});

function test() {
   if ($("#check_option").attr("checked")) {
      $("#option").html(" ");
   } else {
      $("#option").load("index.php", 'execute=pa');
   }
};

window.onload = function(){

   if ($("#check_option").attr("checked")) {
      $("#option").html(" ");
   } else {
      $("#option").load("index.php", 'execute=pa');
   }
};

function change(id, table) {
   $.ajax({
      url:"index.php", 
      data:"id="+id+"&execute=change_status&change_table="+table, 
      success:function(result){
         $("#"+table+id).html(result);
      }
   });
}

function del_item(id, table) {
   var total = Number($('#basketTotal').text())-parseInt($("#baskSumId"+id).text());
   $.ajax({
      url:"index.php", 
      data:"&id="+id+"&execute=change_status&change_table="+table, 
      success:function(result){
         $(".good"+id).html('Товар удален.');
         $('#basketTotal').html(total);
      }
   });
}

function calcqt(id,step){
   var qt = Number($('#qt'+id).text());
   var price = Number($('#priceId'+id).text());

   var sum = qt*price;
   var total = Number($('#basketTotal').text()) - sum;
   qt += step;
   if (qt<1) {qt=1}
   if (qt>9) {qt=9}
   sum = qt*price;
   total += sum;

   $.ajax({
      url:"index.php", 
      data:"qt="+qt+"&id="+id+"&execute=qt", 
      success:function(result){
         if (result == '1') {
            $("#qt"+id).html(qt);
            $("#baskSumId"+id).html(sum+' p.');
            $("#basketTotal").html(total);
         } else {
            $("#baskSumlId"+id).html('disconn. DB');
         }
      }
   });
}